const express = require('express');
const path = require('path');
const multer = require('multer');

const mysqlDb = require('../mysqlDb');

const {nanoid} = require('nanoid');
const router = express.Router();
const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => { // cb = callback
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', async (req, res) => {

    const [news] = await mysqlDb.getConnection().query('SELECT * FROM news');

    res.send(news);
});

router.get('/:id', async (req, res) => {
    const [news] = await mysqlDb.getConnection().query('SELECT * FROM news WHERE id = ?', [req.params.id]);

    const newsItem = news[0];

    res.send(newsItem);
});

router.post('/', upload.single('image'), async (req, res) => {
    const newsItem = req.body;

    if (req.file) {
        newsItem.image = req.file.filename;
    }

    const [result] = await mysqlDb.getConnection().query('INSERT INTO news ' +
        '(title, description, image, date) VALUES (?, ?, ?, NOW())',
        [newsItem.title, newsItem.description, newsItem.image, newsItem.date]);

    res.send({...newsItem, id: result.insertId});
});

router.delete('/:id', async (req, res) => {
    const newsItem = req.body;

    const [result] = await mysqlDb.getConnection().query('DELETE FROM news WHERE id = ?',
        [newsItem.title, newsItem.description, newsItem.image, newsItem.date]);

    res.send({...newsItem, id: result.insertId});
});

router.put('/:id', async (req, res) => {
    const newsItemId = req.params.id;
    const newsItem = req.body;

    if (req.file) {
        newsItem.image = req.file.filename;
    }

    const [result] = await mysqlDb.getConnection().query(
        'UPDATE news SET title=?, description=?, image=?, date=? WHERE id = ?',
        [newsItem.title, newsItem.description, newsItem.image, newsItem.date, newsItemId]
    );

    res.send({...newsItem, id: result.insertId});
});

module.exports = router;