const express = require('express');

const mysqlDb = require('../mysqlDb');

const router = express.Router();

router.get('/', async (req, res) => {
    const [comments] = await mysqlDb.getConnection().query('SELECT * FROM comments');

    res.send(comments);
});

router.get('/:id', async (req, res) => {
    const [comments] = await mysqlDb.getConnection().query('SELECT * FROM comments WHERE id = ?', [req.params.id]);

    const comment = comments[0];

    res.send(comment);
});

router.post('/', async (req, res) => {
    const comment = req.body;

    const [result] = await mysqlDb.getConnection().query('INSERT INTO comments (news_id, author, comment) VALUES (?, ?, ?)',
        [comment.news_id, comment.author, comment.comment]);

    res.send({...comment, id: result.insertId});
});

router.delete('/:id', async (req, res) => {
    const comment = req.body;

    const [result] = await mysqlDb.getConnection().query('DELETE FROM comments WHERE id = ?',
        [comment.news_id, comment.author, comment.comment]);

    res.send({...comment, id: result.insertId});
});

router.put('/:id', async (req, res) => {
    const commentId = req.params.id;
    const comment = req.body;

    const [result] = await mysqlDb.getConnection().query(
        'UPDATE comments SET news_id=?, author=?, comment=? WHERE id = ?',
        [comment.news_id, comment.author, comment.comment, commentId]
    );

    res.send({...comment, id: result.insertId});
});

module.exports = router;